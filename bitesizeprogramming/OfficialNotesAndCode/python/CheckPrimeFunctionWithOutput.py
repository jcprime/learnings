# -*- coding: utf-8 -*-
"""
Created on Sat Nov  4 17:34:09 2017

Dr Amir-Homayoun Javadi
www.javadilab.com
Introduction to Programming for busy students (MATLAB & Python)
https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3037

"""

# in Python you don't need to same the functions in a separate file. 
def CheckPrimeFunctionWithOutput(n):
# This function has one input, and one output to the calling function.
# Additionally, it displays the output in the Command Window. 
# This is very uncommon in functions to display the output directly to the Command Window.
# See CheckPrimeFunctionWithOutputNoDisp.py

	i = 2
	
	if(n == 1):
		print('Non-prime')
		return('Non-prime')
	else:
		while(1):
			if((n % i) == 0):		# (n % i) is similar to mod(n, i)
				print('Non-prime')
				return('Non-prime')
	
				break
			if(i == (n - 1)):
				print('Prime')
				return('Prime')
				break
	
			i = i + 1
	
ResultFromFunction = CheckPrimeFunctionWithOutput(36)
print('Result from Function:', ResultFromFunction)

ResultFromFunction = CheckPrimeFunctionWithOutput(7)
print('Result from Function:', ResultFromFunction)
