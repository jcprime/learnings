marks = [59, 45, 68, 75]; # Semicolon suppresses the output
passes = [];
passindices = [];
for i = 1:length(marks)
    if(marks(i) >= 50)
        passes(end+1) = marks(i);
        passindices(end+1) = i;
    end
end
disp(passes);
disp(passindices);
