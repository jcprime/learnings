function CheckPrimeFunctionNoOutput(n)
% This function has one input, and no output to the calling function.
% Instead it will display the output to the Command Window using disp function.
% This is very uncommon in functions to display the output directly to the Command Window.
% See CheckPrimeFunctionWithOutputNoDisp.m

% Dr Amir-Homayoun Javadi
% www.javadilab.com
% Introduction to Programming for busy students (MATLAB & Python)
% https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3037

i = 2;

if(n == 1)
    disp('Non-prime');
else
    while(1)
        if(mod(n, i) == 0)
            disp('non-prime');
            disp(i);

            break;
        end
        if(i == (n - 1))
            disp('Prime');
            break;
        end

        i = i + 1;
    end
end
