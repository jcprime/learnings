# -*- coding: utf-8 -*-
"""
Created on Wed December 06 17:34:09 2017

Dr Amir-Homayoun Javadi
www.javadilab.com
Bitesize Programming (including MATLAB & Python)
https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3117

"""

# This code receives description of an object such as below,
# and calculates whether it is stable or not.
# In the description of the object it has been assumed that
# the first point begins from zero, and 
# the second point is on the horizontal surface (y = 0) and also
# it is extended on the right hand sie of the origin (x > 0).

import matplotlib.pyplot as plt

# Object = [[0, 0], [3, 0], [8, 3], [6, 5], [2, 6], [0, 0]]			# Unstable
Object = [[0, 0], [3, 0], [8, 3], [6, 5], [2, 6], [-2, 5], [0, 0]]	# Stable

SumSoFarX = 0
SumSoFarY = 0
for i in range(len(Object)):
    SumSoFarX = SumSoFarX + Object[i][0]
    SumSoFarY = SumSoFarY + Object[i][1]

CentreofMassX = (SumSoFarX / len(Object))
CentreofMassY = (SumSoFarY / len(Object))

if(CentreofMassX > Object[1][0]):
    print('Unstable')
else:
    print('Stable')

# you can use this,
ObjectXLocation = [Object[i][0] for i in range(len(Object))]
ObjectYLocation = [Object[i][1] for i in range(len(Object))]

plt.plot(ObjectXLocation, ObjectYLocation, '*:b')
plt.plot(CentreofMassX, CentreofMassY, '*r')

# or alternatively this

import numpy as np
Object = np.array(Object)

plt.plot(Object[:, 0], Object[:, 1], '*:b')
plt.plot(CentreofMassX, CentreofMassY, '*r')
