# -*- coding: utf-8 -*-
"""
Created on Sat Nov  4 17:34:09 2017

Dr Amir-Homayoun Javadi
www.javadilab.com
Introduction to Programming for busy students (MATLAB & Python)
https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3037

"""

# in Python you don't need to same the functions in a separate file. 
def CheckPrimeFunctionNoOutput(n):
# This function has one input, and no output to the calling function.
# Instead it will display the output to the Command Window using disp function.
# This is very uncommon in functions to display the output directly to the Command Window.
# See CheckPrimeFunctionWithOutputNoDisp.py

	i = 2
	
	if(n == 1):
	    print('Non-prime')
	else:
	    while(1):
	        if((n % i) == 0):		# (n % i) is similar to mod(n, i)
	            print('non-prime')
	            print(i)
	
	            break
	        if(i == (n - 1)):
	            print('Prime')
	            break
	
	        i = i + 1
	
CheckPrimeFunctionNoOutput(36)
CheckPrimeFunctionNoOutput(7)
