% Dr Amir-Homayoun Javadi
% www.javadilab.com
% Bitesize Programming (including MATLAB & Python)
% https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3117

% In this approach we check the possible payments from SMALLER amount to
% BIGGER amount.

List = [1, 2, 5, 10, 20, 50];
n = 19;

i = length(List);
while(n > 0)
	while(List(i) > n)
		i = i - 1;
	end

	disp(List(i));
	n = n - List(i);
end
