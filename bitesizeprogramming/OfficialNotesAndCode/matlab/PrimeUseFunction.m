% Dr Amir-Homayoun Javadi
% www.javadilab.com
% Introduction to Programming for busy students (MATLAB & Python)
% https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3037

% CheckPrimeFunctionNoOutput(36);
% CheckPrimeFunctionNoOutput(51);
% CheckPrimeFunctionNoOutput(136548873);
% CheckPrimeFunctionNoOutput(67);

% U = CheckPrimeFunctionWithOutput(36);
% disp(['this is the output of the calling function ' U]);

T = CheckPrimeFunctionWithOutputNoDisp(36);
disp(['This is the new result: ' T]);
