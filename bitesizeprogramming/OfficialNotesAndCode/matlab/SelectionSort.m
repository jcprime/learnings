% Dr Amir-Homayoun Javadi
% www.javadilab.com
% Bitesize Programming (including MATLAB & Python)
% https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3117

List = [6, 12, 9, 4, 8, 3, 13, 7];

% a:b:c is a sequence of numbers beginning from a, counting with step of b
% up to c. For counting downward, you need to indicate step -1.
for SubBlockLastIndex = length(List):-1:1
	disp(List);

	% This is the sub-block for finding the index of maximum value in the
	% sub-list as indicated by SubBlockLastIndex.
	MaxSoFar = 0;		% assuming that all the numbers are positive
	MaxSoFarIndex = 0;	% MaxSoFar keeps the value, MaxSoFarIndex keeps the
						% index of the maximum value.
	for CountList = 1:SubBlockLastIndex
		if(List(CountList) > MaxSoFar)
			MaxSoFar = List(CountList);
			MaxSoFarIndex = CountList;
		end
	end

	% This is the sub-block for swapping the maximum value as indicated by
	% MaxSoFar and MaxSoFarIndex and the last number in the sub-list as
	% indicated by SubBlockLastIndex
	Temp = List(SubBlockLastIndex);
	List(SubBlockLastIndex) = List(MaxSoFarIndex);
	List(MaxSoFarIndex) = Temp;
end

disp('Final sorted list:');
disp(List);
