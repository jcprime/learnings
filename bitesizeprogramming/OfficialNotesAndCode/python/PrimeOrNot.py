# -*- coding: utf-8 -*-
"""
Created on Sat Nov  4 17:34:09 2017

Dr Amir-Homayoun Javadi
www.javadilab.com
Introduction to Programming for busy students (MATLAB & Python)
https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3037

"""

n = 15
i = 2

if(n == 1):
    print('Non-prime')
else:
    while(1):
        if((n % i) == 0):
            print('non-prime')
            print(i)

            break
        if(i == (n - 1)):
            print('Prime')
            break

        i = i + 1
