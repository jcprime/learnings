% Dr Amir-Homayoun Javadi
% www.javadilab.com
% Bitesize Programming (including MATLAB & Python)
% https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3117

% This code receives description of an object such as below,
% and calculates whether it is stable or not.
% In the description of the object it has been assumed that
% the first point begins from zero, and
% the second point is on the horizontal surface (y = 0) and also
% it is extended on the right hand sie of the origin (x > 0).

% Object = [0, 0; 3, 0; 8, 3; 6, 5; 2, 6; 0, 0];		% Stable
Object = [0, 0; 3, 0; 8, 3; 6, 5; 2, 6; -2, 5; 0, 0]; 	% Unstable

SumSoFarX = 0;
SumSoFarY = 0;
for i = 1:length(Object)
    SumSoFarX = SumSoFarX + Object(i, 1);
    SumSoFarY = SumSoFarY + Object(i, 2);
end

CentreofMassX = (SumSoFarX / (length(Object) - 1));		% we subtract 1 because we have (0, 0) at the end.
CentreofMassY = (SumSoFarY / (length(Object) - 1));

if(CentreofMassX > Object(2, 1))
    disp('Unstable');
else
    disp('Stable');
end

clf; hold on;
plot(Object(:, 1), Object(:, 2), '*:b')
plot(CentreofMassX, CentreofMassY, '*r');
