# -*- coding: utf-8 -*-
"""
Created on Sat Nov  4 17:34:09 2017

Dr Amir-Homayoun Javadi
www.javadilab.com
Introduction to Programming for busy students (MATLAB & Python)
https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3037

"""

# in Python you don't need to same the functions in a separate file. 
def CheckPrimeFunctionWithOutputNoDisp(n):
# This function has one input, and one output to the calling function.
# In contrast to CheckPrimeFunctionNoOutput.m and CheckPrimeFunctionWithOutput.m, 
# it does not display anything in the Command Window. 
# This is the common way of using functions -- to send all the output to the calling function.

	i = 2
	
	if(n == 1):
		print('Non-prime')
		return('Non-prime')
	else:
		while(1):
			if((n % i) == 0):		# (n % i) is similar to mod(n, i)
				return('Non-prime')
	
				break
			if(i == (n - 1)):
				return('Prime')
				break
	
			i = i + 1

ResultFromFunction = CheckPrimeFunctionWithOutputNoDisp(36)
print('Result from Function:', ResultFromFunction)

ResultFromFunction = CheckPrimeFunctionWithOutputNoDisp(7)
print('Result from Function:', ResultFromFunction)
