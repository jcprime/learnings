#!/bin/bash
# For some reason, futurelearn-dl.py downloaded the weeks with week 6 in the week1 directory
# and everything else in order (week 1 in week2 directory, etc, and then weeks 7 and 8 where
# they should be), so this fixes it

for num in {1..6}; do
    if [[ "$num" -eq "1" ]]; then
        mv week"$num" weekTMP;
    else
        mv week"$num" week"$((num-1))"
    fi
done
mv weekTMP week6
