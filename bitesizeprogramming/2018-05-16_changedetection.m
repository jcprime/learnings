listA = [5, 8, 4, 2, 1, 9, 7];
listB = [5, 8, 0, 2, 1, 9, 7];

% Assuming the two lists are the same length!
for i = 1:len(listA)
    if listA(i) ~= listB(i)
        disp("Change at index", i, ": ", listA[i], "-->", listB[i]);
    end
end
