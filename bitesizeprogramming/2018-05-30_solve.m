% The below MUST GO IN A SEPARATE FILE!
function y = f(x)
% y = 5 * x - 10;

x = 0;
Step = 1;
y = [];
% Alternatively yPrevious = f(x);

while(1)
    x += Step;
    y(end+1) = 5 * x - 10;
    % Alternatively y = f(x);
    if ((y(end) > 0 && y(end -1) < 0) || (y(end) < 0 && y(end - 1) > 0))
    % Alternatively if(sign(yPrevious) ~= sign(y))
        % Solution interval
        Step = (-0.5) * Step;
        disp(Step)
        % break
    end
    if(abs*y) < 0.000001)
        break
    end
    yPrevious = y;
end

disp(x)
