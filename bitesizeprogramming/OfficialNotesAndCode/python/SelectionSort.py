# -*- coding: utf-8 -*-
"""
Created on Sat Nov  4 17:34:09 2017

Dr Amir-Homayoun Javadi
www.javadilab.com
Bitesize Programming (including MATLAB & Python)
https://doctoral-skills.ucl.ac.uk/course-details.pht?course_ID=3117

"""

List = [6, 12, 9, 4, 8, 3, 13, 7]

# range(a, b, c) is a sequence of numbers beginning from a, counting with step 
# of c up to b, exclusive. For counting downward, you need to indicate step -1.
# Here we begin from len(List) - 1 as lists in Python begin from zero. 
for SubBlockLastIndex in range(len(List) - 1, 0, -1):
	print(List)
	
	# This is the sub-block for finding the index of maximum value in the
	# sub-list as indicated by SubBlockLastIndex.
	MaxSoFar = 0 	 	 	# assuming that all the numbers are positive
	MaxSoFarIndex = 0	 	# MaxSoFar keeps the value, MaxSoFarIndex keeps the 
							# index of the maximum value. 
	for CountList in range(0, SubBlockLastIndex + 1):
		if(List[CountList] > MaxSoFar):
			MaxSoFar = List[CountList]
			MaxSoFarIndex = CountList
	
	# This is the sub-block for swapping the maximum value as indicated by
	# MaxSoFar and MaxSoFarIndex and the last number in the sub-list as 
	# indicated by SubBlockLastIndex
	Temp = List[SubBlockLastIndex]
	List[SubBlockLastIndex] = List[MaxSoFarIndex]
	List[MaxSoFarIndex] = Temp

print('Final sorted list:')
print(List)
